﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace IMDb.Domain.Extensions
{
    public static class StringExtensions
    {
        public static bool Isfilled(this string str)
            => !(string.IsNullOrEmpty(str) || str.Trim().Length == 0);

        public static string ToMD5(this string senha)
        {
            if (!senha.Isfilled())
                return senha;

            senha = string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(senha)).Select(s => s.ToString("x2")));
            return senha.ToUpper();
        }
    }
}
