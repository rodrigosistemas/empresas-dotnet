﻿using IMDb.Domain.Seletores;

namespace IMDb.Domain.Service
{
    public interface IAtorService : IService<AtorDomain, AtorSeletor>
    {

    }
}
