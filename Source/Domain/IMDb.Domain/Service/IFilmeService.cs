﻿using IMDb.Domain.Seletores;
using System;

namespace IMDb.Domain.Service
{
    public interface IFilmeService : IService<FilmeDomain, FilmeSeletor>
    {
        FilmeDomain GetFilmeDetalheById(Guid id);
    }
}
