﻿using IMDb.Domain.Seletores;

namespace IMDb.Domain.Service
{
    public interface IGeneroService : IService<GeneroDomain, GeneroSeletor>
    {

    }
}
