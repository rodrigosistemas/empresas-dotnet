﻿using IMDb.Domain.Seletores;
using System.Collections.Generic;

namespace IMDb.Domain.Service
{
    public interface IUsuarioService : IService<UsuarioDomain, UsuarioSeletor>
    {
        UsuarioDomain Login(UsuarioDomain domain);
        IEnumerable<UsuarioDomain> GetListPerfilUsuario(UsuarioComunSeletor seletor);
        int CountPerfilUsuario(SeletorBase seletor);
    }
}
