﻿namespace IMDb.Domain.Service
{
    public interface ITokenService {
        string GenerateToken(UsuarioDomain user);
    }
}
