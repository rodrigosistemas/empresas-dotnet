﻿using IMDb.Domain.Seletores;

namespace IMDb.Domain.Service
{
    public interface IDiretorService : IService<DiretorDomain, DiretorSeletor>
    {

    }
}
