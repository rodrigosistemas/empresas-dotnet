﻿using IMDb.Domain.Seletores;

namespace IMDb.Domain.Service
{
    public interface IAvaliacaoService : IService<AvaliacaoDomain, AvaliacaoSeletor>
    {

    }
}
