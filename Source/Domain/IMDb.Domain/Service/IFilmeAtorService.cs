﻿using IMDb.Domain.Seletores;

namespace IMDb.Domain.Service
{
    public interface IFilmeAtorService : IService<FilmeAtorDomain, FilmeAtorSeletor>
    {

    }
}
