﻿using IMDb.Domain.Repository.Abstract;
using IMDb.Domain.Seletores;
using System.Collections.Generic;

namespace IMDb.Domain.Repository
{
    public interface IAtorRepository : IRepositorySeletorBase<AtorDomain, AtorSeletor>
    {
    }
}
