﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Domain.Repository.Abstract
{
    public interface IRepositoryBase<TDomain>
       where TDomain : DomainBase
    {
        Guid InsertWithReturningId(TDomain obj);

        TDomain InsertWithReturningObject(TDomain obj);

        void Insert(TDomain obj);

        IEnumerable<Guid> InsertMany(IEnumerable<TDomain> itens);

        IEnumerable<TDomain> InsertManyReturningObject(IEnumerable<TDomain> itens);

        void Update(TDomain obj);

        void UpdateMany(IEnumerable<TDomain> itens);

        void Delete(TDomain obj);

        void DeleteMany(IEnumerable<TDomain> items);

        IQueryable<TDomain> Get();

        TDomain GetById(Guid id);

        IEnumerable<TDomain> GetByIds(params Guid[] id);

        void Save();
    }
}
