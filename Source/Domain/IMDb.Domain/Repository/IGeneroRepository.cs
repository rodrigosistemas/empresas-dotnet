﻿using IMDb.Domain.Repository.Abstract;
using IMDb.Domain.Seletores;

namespace IMDb.Domain.Repository
{
    public interface IGeneroRepository : IRepositorySeletorBase<GeneroDomain, GeneroSeletor>
    {

    }
}
