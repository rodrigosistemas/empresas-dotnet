﻿using IMDb.Domain.Repository.Abstract;
using IMDb.Domain.Seletores;
using System;
using System.Collections.Generic;

namespace IMDb.Domain.Repository
{
    public interface IFilmeRepository : IRepositorySeletorBase<FilmeDomain, FilmeSeletor>
    {
        IEnumerable<FilmeDomain> GetListMedia(FilmeSeletor seletor);
        FilmeDomain GetFilmeDetalheById(Guid id);
    }
}
