﻿using IMDb.Domain.Repository.Abstract;
using IMDb.Domain.Seletores;
using System;
using System.Collections.Generic;

namespace IMDb.Domain.Repository
{
    public interface IUsuarioRepository : IRepositorySeletorBase<UsuarioDomain, UsuarioSeletor>
    {
        IEnumerable<UsuarioDomain> GetListDelete(UsuarioSeletor usuario);
        void Desativar(Guid id);
    }
}
