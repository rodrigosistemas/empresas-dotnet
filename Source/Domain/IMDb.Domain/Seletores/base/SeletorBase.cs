﻿using System;

namespace IMDb.Domain.Seletores
{
    public abstract class SeletorBase : ISeletor
    {
        public SeletorBase()
        {
            Id = Guid.Empty;
            Ativo = true;
            OrderBy = "Id";
            Pagina = 1;
            RegistroPorPagina = 10;
        }

        public Guid Id { get; set; }
        public bool Ativo { get; set; }
        public int Pagina { get; set; }

        public int RegistroPorPagina { get; set; }

        public string OrderBy { get; set; }

        public string Coringa { get; set; }

        public bool IsSelectedAll { get; set; }
    }
}
