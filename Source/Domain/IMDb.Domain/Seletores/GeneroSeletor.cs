﻿namespace IMDb.Domain.Seletores
{
    public class GeneroSeletor : SeletorBase
    {
        public string Nome { get; set; }
    }
}
