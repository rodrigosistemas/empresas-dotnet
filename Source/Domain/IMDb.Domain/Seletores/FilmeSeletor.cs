﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain.Seletores
{
    public class FilmeSeletor : SeletorBase
    {
        public string Nome { get; set; }
        public Guid? IdDiretor { get; set; }
        public Guid? IdGenero { get; set; }
        public List<Guid> Atores { get; set; }
    }
}
