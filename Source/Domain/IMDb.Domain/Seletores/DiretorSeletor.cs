﻿namespace IMDb.Domain.Seletores
{
    public class DiretorSeletor : SeletorBase
    {
        public string Nome { get; set; }
    }
}
