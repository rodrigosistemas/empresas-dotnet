﻿using System;

namespace IMDb.Domain.Seletores
{
    public class FilmeAtorSeletor : SeletorBase
    {
        public Guid? IdFilme { get; set; }
        public Guid? IdAtor { get; set; }
    }
}
