﻿using IMDb.Domain.Enums;

namespace IMDb.Domain.Seletores
{
    public class UsuarioSeletor : SeletorBase
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public PerfilAcesso? PerfilAcesso { get; set; }
    }
}
