﻿using System;

namespace IMDb.Domain.Seletores
{
    public class AvaliacaoSeletor : SeletorBase
    {
        public Guid? IdFilme { get; set; }
    }
}
