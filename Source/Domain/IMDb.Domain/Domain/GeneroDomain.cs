﻿using System.Collections.Generic;

namespace IMDb.Domain
{
    public class GeneroDomain : DomainBase
    {
        public GeneroDomain()
        {
            Ativo = true;
        }

        public string Nome { get; set; }
        public IEnumerable<FilmeDomain> Filmes { get; set; }
    }
}
