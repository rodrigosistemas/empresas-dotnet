﻿using IMDb.Domain.Extensions;
using prmToolkit.NotificationPattern;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Domain
{
    public class FilmeDomain : DomainBase
    {
        public FilmeDomain()
        {
            Ativo = true;
        }

        public FilmeDomain(string nome, Guid idGenero, Guid idDiretor, IEnumerable<AtorDomain> atores)
        {
            ValidarNome(nome);

            Nome = nome;
            IdGenero = idGenero;
            IdDiretor = idDiretor;
            Atores = atores;
            Ativo = true;
        }

        public string Nome { get; set; }
        public Guid IdGenero { get; set; }
        public Guid IdDiretor { get; set; }
        public GeneroDomain Genero { get; set; }
        public DiretorDomain Diretor { get; set; }
        public IEnumerable<AtorDomain> Atores { get; set; }
        public IEnumerable<AvaliacaoDomain> Avaliacoes { get; set; }
        public decimal Avaliacao
            => Avaliacoes != null && Avaliacoes.Any() ? decimal.Round((decimal)Avaliacoes.Average(x=> x.Nota), 2) : 0;

        public void ValidarNome(string nome)
           => new AddNotifications<FilmeDomain>(this)
                .IfNullOrWhiteSpace(nome, nameof(Nome), "Nome é obrigatória")
                .IfLengthGreaterThan(nome, max: 80, nameof(Nome), "Comprimento máximo do nome é 80 caracteres");

        public void UpdateNome(string nome)
        {
            if (nome.Isfilled())
            {
                ValidarNome(nome);
                Nome = nome;
            }
        }
    }
}
