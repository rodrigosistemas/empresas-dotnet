﻿using IMDb.Domain.Enums;
using IMDb.Domain.Extensions;
using prmToolkit.NotificationPattern;

namespace IMDb.Domain
{
    public class UsuarioDomain : DomainBase
    {
        public UsuarioDomain()
        {
            Ativo = true;
        }

        public UsuarioDomain(string nome, string email, string senha, PerfilAcesso perfilAcesso, bool ativo = true)
        {
            ValidarNome(nome);
            ValidarEmail(email);
            ValidarSenha(senha);

            Nome = nome;
            Email = email;
            Senha = senha;
            PerfilAcesso = perfilAcesso;
            Ativo = ativo;
        }

        public UsuarioDomain(string email, string senha)
        {
            ValidarEmail(email);
            ValidarSenha(senha);

            Email = email;
            Senha = senha;
        }

        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public PerfilAcesso PerfilAcesso { get; set; }

        public void ValidarNome(string nome)
           => new AddNotifications<UsuarioDomain>(this)
                  .IfNullOrWhiteSpace(nome, nameof(Nome), "Nome é obrigatória")
                  .IfLengthGreaterThan(nome, max: 80, nameof(Nome), "Comprimento máximo do nome é 80 caracteres");

       public void ValidarEmail(string email)
          => new AddNotifications<UsuarioDomain>(this)
                .IfNullOrWhiteSpace(email, nameof(Email), "Email é obrigatória")
                .IfLengthGreaterThan(email, max: 80, nameof(Email), "Comprimento máximo do email é 80 caracteres");

        public void ValidarSenha(string senha)
           => new AddNotifications<UsuarioDomain>(this)
                 .IfNullOrWhiteSpace(senha, nameof(Senha), "Senha é obrigatória")
                 .IfNullOrInvalidLength(senha, min: 32, max: 32 , nameof(Senha), "Senha inválida");

        public void UpdateNome(string nome)
        {
            if (nome.Isfilled())
            {
                ValidarNome(nome);
                Nome = nome;
            }
        }

        public void UpdateEmail(string email)
        {
            if (email.Isfilled())
            {
                ValidarEmail(email);
                Email = email;
            }
        }

        public void UpdateSenha(string senha)
        {
            if (senha.Isfilled())
            {
                ValidarSenha(senha);
                Senha = senha;
            }
        }

        public void UpdatePerfilAcesso(PerfilAcesso perfilAcesso)
        {
            PerfilAcesso = perfilAcesso;
        }
    }
}
