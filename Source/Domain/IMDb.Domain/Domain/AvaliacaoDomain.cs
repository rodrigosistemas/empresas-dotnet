﻿using prmToolkit.NotificationPattern;
using System;

namespace IMDb.Domain
{
    public class AvaliacaoDomain : DomainBase
    {
        public AvaliacaoDomain()
        {
            Ativo = true;
        }

        public AvaliacaoDomain(Guid idUsuario, Guid idFilme, int nota, bool ativo = true)
        {
            ValidarIdUsurio(idUsuario);
            ValidarIdFilme(idFilme);
            ValidarNota(nota);

            IdUsuario = idUsuario;
            IdFilme = idFilme;
            Nota = nota;
            Ativo = ativo;
        }

        public Guid IdUsuario { get; set; }
        public Guid IdFilme { get; set; }
        public int Nota { get; set; }

        public void ValidarIdUsurio(Guid idUsuario)
            => new AddNotifications<AvaliacaoDomain>(this)
               .IfTrue(idUsuario == Guid.Empty, nameof(IdUsuario), "Usuário é obrigatório");

        public void ValidarIdFilme(Guid idFilme)
            => new AddNotifications<AvaliacaoDomain>(this)
               .IfTrue(idFilme == Guid.Empty, nameof(IdFilme), "Filme é obrigatório");

        public void ValidarNota(int nota)
           => new AddNotifications<AvaliacaoDomain>(this)
              .IfNotRange(nota, 0, 4, nameof(Nota), "Nota inválida, a nota deve estar entra 0 e 4");
    }
}
