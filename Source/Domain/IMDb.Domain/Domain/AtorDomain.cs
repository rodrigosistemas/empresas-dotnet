﻿using System;
using System.Collections.Generic;

namespace IMDb.Domain
{
    public class AtorDomain : DomainBase
    {
        public AtorDomain()
        {
            Ativo = true;
        }

        public AtorDomain(Guid id) : base(id)
        {
            Ativo = true;
        }

        public string Nome { get; set; }
        public IEnumerable<FilmeDomain> Filmes { get; set; }
    }
}
