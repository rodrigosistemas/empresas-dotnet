﻿using System.Collections.Generic;

namespace IMDb.Domain
{
    public class DiretorDomain : DomainBase
    {
        public DiretorDomain()
        {
            Ativo = true;
        }

        public string Nome { get; set; }
        public IEnumerable<FilmeDomain> Filmes { get; set; }
    }
}
