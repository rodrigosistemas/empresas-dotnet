﻿using System;

namespace IMDb.Domain
{
    public class FilmeAtorDomain : DomainBase
    {
        public FilmeAtorDomain()
        {
                
        }

        public FilmeAtorDomain(Guid idAtor, Guid idFilme)
        {
            IdAtor = idAtor;
            IdFilme = idFilme;
        }

        public Guid IdAtor { get; set; }
        public Guid IdFilme { get; set; }
    }
}
