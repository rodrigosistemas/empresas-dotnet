﻿using prmToolkit.NotificationPattern;
using System;

namespace IMDb.Domain
{
    public class DomainBase : Notifiable
    {
        public DomainBase() { }

        public DomainBase(Guid id)
            => Id = id;

        public Guid Id { get; set; }
       public bool Ativo { get; set; }
    }
}
