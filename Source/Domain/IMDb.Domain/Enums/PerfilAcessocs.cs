﻿namespace IMDb.Domain.Enums
{
    public enum PerfilAcesso
    {
        Administrador = 1,
        Usuario = 2
    }
}
