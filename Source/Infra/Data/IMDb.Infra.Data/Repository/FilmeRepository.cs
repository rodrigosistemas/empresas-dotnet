﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class FilmeRepository : RepositoryBase<FilmeEntity, FilmeDomain, FilmeSeletor>, IFilmeRepository
    {
        public FilmeRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public IQueryable<FilmeEntity> CreateQueryFull()
                => _context.Filme
                           .Include(x => x.Diretor)
                           .Include(x => x.Genero)
                           .Include(x => x.FilmesAtor)
                               .ThenInclude(x => x.Ator)
                           .Include(x => x.Avaliacoes).AsNoTracking();

        public override IQueryable<FilmeEntity> CreateParameters(FilmeSeletor seletor, IQueryable<FilmeEntity> query)
        {           
            if (!string.IsNullOrEmpty(seletor.Nome))
                query = query.Where(x => x.Nome.Contains(seletor.Nome));

            if (seletor.IdDiretor.HasValue)
                query = query.Where(x => x.IdDiretor == seletor.IdDiretor.Value);

            if (seletor.IdGenero.HasValue)
                query = query.Where(x => x.IdGenero == seletor.IdGenero.Value);

            if (seletor.Atores != null && seletor.Atores.Any())
            {
                query = query.Include(x=> x.FilmesAtor);
                query = query.Where(x => x.FilmesAtor.Any(y=> seletor.Atores.Contains(y.IdAtor)));
            }

            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }

        public IEnumerable<FilmeDomain> GetListMedia(FilmeSeletor seletor)
        {
            IQueryable<FilmeEntity> query = this.CreateQuery();

            query = this.CreateParameters(seletor, query);
            query = query.Include(x=> x.Avaliacoes);

            var filmeAvaliacao = query.Select(filme => new { filme, qtdVotos = filme.Avaliacoes.Count() })
                                      .OrderByDescending(x=> x.qtdVotos)
                                      .ThenBy(x=> x.filme.Nome);

            int skip = ((seletor.Pagina - 1) * seletor.RegistroPorPagina);
            int take = seletor.RegistroPorPagina;

            var list = filmeAvaliacao.Skip(skip).Take(take).Select(x=> x.filme);

            return _mapper.Map<IEnumerable<FilmeDomain>>(list);
        }

        public FilmeDomain GetFilmeDetalheById(Guid id)
        {
            var query = CreateQueryFull();

            var filme = query.FirstOrDefault(x => x.Id == id);
            var filmedomain = MapperToDomain(filme);
            filmedomain.Atores = _mapper.Map<IEnumerable<AtorDomain>>(filme.FilmesAtor.Select(x => x.Ator));

            return filmedomain;
        }
    }
}
