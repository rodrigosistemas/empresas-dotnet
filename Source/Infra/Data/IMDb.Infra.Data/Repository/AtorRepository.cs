﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class AtorRepository : RepositoryBase<AtorEntity, AtorDomain, AtorSeletor>, IAtorRepository
    {
        public AtorRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public override IQueryable<AtorEntity> CreateParameters(AtorSeletor seletor, IQueryable<AtorEntity> query)
        {
            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }
    }
}
