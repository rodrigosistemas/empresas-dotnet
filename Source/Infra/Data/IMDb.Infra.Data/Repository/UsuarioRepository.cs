﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class UsuarioRepository : RepositoryBase<UsuarioEntity, UsuarioDomain, UsuarioSeletor>, IUsuarioRepository
    {
        public UsuarioRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public override IQueryable<UsuarioEntity> CreateParameters(UsuarioSeletor seletor, IQueryable<UsuarioEntity> query)
        {
            if (!string.IsNullOrEmpty(seletor.Nome))
                query = query.Where(x => x.Nome.Contains(seletor.Nome));

            if (!string.IsNullOrEmpty(seletor.Email))
                query = query.Where(x => x.Email == seletor.Email);

            if (!string.IsNullOrEmpty(seletor.Senha))
                query = query.Where(x => x.Senha == seletor.Senha);

            if (seletor.PerfilAcesso.HasValue)
                query = query.Where(x => x.PerfilAcesso == seletor.PerfilAcesso.Value);

            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }

        public IEnumerable<UsuarioDomain> GetListDelete(UsuarioSeletor usuario)
        {
            var query = CreateQuery();

            return _mapper.Map<IEnumerable<UsuarioDomain>>(query.ToList());
        }

        public void Desativar(Guid id)
        {
            var user = _context.Usuario.Find(id);
            
            if(user != null)
            {
                user.Ativo = false;
                _context.Usuario.Update(user);
                Save();
            }
        }
    }
}
