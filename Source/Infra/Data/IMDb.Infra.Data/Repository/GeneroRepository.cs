﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class GeneroRepository : RepositoryBase<GeneroEntity, GeneroDomain, GeneroSeletor>, IGeneroRepository
    {
        public GeneroRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public override IQueryable<GeneroEntity> CreateParameters(GeneroSeletor seletor, IQueryable<GeneroEntity> query)
        {
            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }
    }
}
