﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class FilmeAtorRepository : RepositoryBase<FilmeAtorEntity, FilmeAtorDomain, FilmeAtorSeletor>, IFilmeAtorRepository
    {
        public FilmeAtorRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public override IQueryable<FilmeAtorEntity> CreateParameters(FilmeAtorSeletor seletor, IQueryable<FilmeAtorEntity> query)
        {           

            if (seletor.IdAtor.HasValue)
                query = query.Where(x => x.IdAtor == seletor.IdAtor.Value);

            if (seletor.IdFilme.HasValue)
                query = query.Where(x => x.IdFilme == seletor.IdFilme.Value);

            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }
    }
}
