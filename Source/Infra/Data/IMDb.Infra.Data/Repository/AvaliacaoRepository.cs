﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class AvaliacaoRepository : RepositoryBase<AvaliacaoEntity, AvaliacaoDomain, AvaliacaoSeletor>, IAvaliacaoRepository
    {
        public AvaliacaoRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public override IQueryable<AvaliacaoEntity> CreateParameters(AvaliacaoSeletor seletor, IQueryable<AvaliacaoEntity> query)
        {
            if(seletor.IdFilme.HasValue)
            query = query.Where(x => x.IdFilme == seletor.IdFilme.Value);

            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }
    }
}
