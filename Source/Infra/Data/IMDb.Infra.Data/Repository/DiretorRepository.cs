﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository.Abstract;
using System.Linq;

namespace IMDb.Infra.Data.Repository
{
    public class DiretorRepository : RepositoryBase<DiretorEntity, DiretorDomain, DiretorSeletor>, IDiretorRepository
    {
        public DiretorRepository(IUnitOfWork uow, IMapper mapper) : base(uow, mapper) { }

        public override IQueryable<DiretorEntity> CreateParameters(DiretorSeletor seletor, IQueryable<DiretorEntity> query)
        {
            query = query.Where(x => x.Ativo == seletor.Ativo);

            return query;
        }
    }
}
