﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Infra.Data.Entities
{
    [Table("FilmeAtor", Schema = "dbo")]
    public class FilmeAtorEntity : EntityBase
    {
        [Column("IdAtor")]
        public Guid IdAtor { get; set; }
        [Column("IdFilme")]
        public Guid IdFilme { get; set; }

        [ForeignKey("IdAtor")]
        public virtual AtorEntity Ator { get; set; }

        [ForeignKey("IdFilme")]
        public virtual FilmeEntity Filme { get; set; }
    }
}
