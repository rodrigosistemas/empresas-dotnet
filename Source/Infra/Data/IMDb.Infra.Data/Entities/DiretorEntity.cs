﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Infra.Data.Entities
{
    [Table("Diretor", Schema = "dbo")]
    public class DiretorEntity : EntityBase
    {
        [Column("Nome", TypeName = "VARCHAR(80)")]
        [Required]
        public string Nome { get; set; }

        public virtual ICollection<FilmeEntity> Filmes { get; set; }
    }
}
