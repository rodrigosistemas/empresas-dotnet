﻿using IMDb.Domain.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Infra.Data.Entities
{
    [Table("Usuario", Schema = "dbo")]
    public class UsuarioEntity : EntityBase
    {
        [Column("Nome", TypeName = "VARCHAR(80)")]
        [Required]
        public string Nome { get; set; }

        [Column("Email", TypeName = "VARCHAR(80)")]
        [Required]
        public string Email { get; set; }

        [Column("Senha", TypeName = "VARCHAR(32)")]
        [StringLength(maximumLength: 32, MinimumLength= 32)]
        [Required]
        public string Senha { get; set; }

        [Column("PerfilAcesso")]
        [Required]
        public PerfilAcesso PerfilAcesso { get; set; }

        public virtual ICollection<AvaliacaoEntity> Avaliacoes { get; set; }
    }
}
