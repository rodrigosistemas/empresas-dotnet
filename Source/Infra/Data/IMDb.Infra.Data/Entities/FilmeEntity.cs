﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Infra.Data.Entities
{
    [Table("Filme", Schema = "dbo")]
    public class FilmeEntity : EntityBase
    {
        [Column("Nome", TypeName = "VARCHAR(80)")]
        [Required]
        public string Nome { get; set; }

        [Column("IdGenero")]
        public Guid IdGenero { get; set; }

        [Column("IdDiretor")]
        public Guid IdDiretor { get; set; }

        [ForeignKey("IdGenero")]
        public virtual GeneroEntity Genero { get; set; }

        [ForeignKey("IdDiretor")]
        public virtual DiretorEntity Diretor { get; set; }

        public virtual ICollection<FilmeAtorEntity> FilmesAtor { get; set; }

        public virtual ICollection<AvaliacaoEntity> Avaliacoes { get; set; }
    }
}
