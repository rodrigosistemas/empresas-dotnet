﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Infra.Data.Entities
{
    [Table("Ator", Schema = "dbo")]
    public class AtorEntity : EntityBase
    {
        [Column("Nome", TypeName = "VARCHAR(80)")]
        [Required]
        public string Nome { get; set; }

        public virtual ICollection<FilmeAtorEntity> FilmesAtor { get; set; }
    }
}
