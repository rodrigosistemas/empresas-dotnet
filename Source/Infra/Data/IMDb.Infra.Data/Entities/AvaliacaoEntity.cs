﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IMDb.Infra.Data.Entities
{
    [Table("Avaliacao", Schema = "dbo")]
    public class AvaliacaoEntity : EntityBase
    {
        [Column("IdUsuario")]
        public Guid IdUsuario { get; set; }
        [Column("IdFilme")]
        public Guid IdFilme { get; set; }
        [Required]
        [Range(0,4)]
        public int Nota { get; set; }

        [ForeignKey("IdFilme")]
        public virtual FilmeEntity Filme { get; set; }
        [ForeignKey("IdUsuario")]
        public virtual UsuarioEntity Usuario { get; set; }
    }
}
