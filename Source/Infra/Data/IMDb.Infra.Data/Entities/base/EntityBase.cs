﻿using System;
using System.ComponentModel.DataAnnotations;

namespace IMDb.Infra.Data.Entities
{
    public class EntityBase
    {
       [Key]
       public Guid Id { get; set; }
       
       [Required]
       public bool Ativo { get; set; }
    }
}
