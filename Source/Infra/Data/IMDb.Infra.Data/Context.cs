﻿using IMDb.Domain.Enums;
using IMDb.Infra.Data.Entities;
using IMDb.Infra.Data.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace IMDb.Infra.Data
{
    public class Context : DbContext, IContext
    {
        public Context(DbContextOptions<Context> options)
        : base(options)
        {
        }
        public virtual DbSet<UsuarioEntity> Usuario { get; set; }
        public virtual DbSet<AtorEntity> Ator { get; set; }
        public virtual DbSet<DiretorEntity> Diretor { get; set; }
        public virtual DbSet<FilmeEntity> Filme { get; set; }
        public virtual DbSet<GeneroEntity> Genero { get; set; }
        public virtual DbSet<FilmeAtorEntity> FilmeAtor { get; set; }

        public virtual DbContext DbContext => this;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           modelBuilder.Entity<UsuarioEntity>().HasData(new UsuarioEntity{
                Id = Guid.NewGuid(),
                Ativo = true,
                Nome = "Rodrigo",
                Email = "rodrigo.sistemas.infor@gmail.com",
                PerfilAcesso = PerfilAcesso.Administrador,
                //senha: 123456
                Senha = "E10ADC3949BA59ABBE56E057F20F883E"
           });

           modelBuilder.Entity<AtorEntity>().HasData(new AtorEntity {
                Id = Guid.NewGuid(),
                Ativo = true,
                Nome = "Rodrigo teste 1"
           });

           modelBuilder.Entity<DiretorEntity>().HasData(new DiretorEntity {
                Id = Guid.NewGuid(),
                Ativo = true,
                Nome = "Rodrigo teste 1"
           });

            modelBuilder.Entity<GeneroEntity>().HasData(new GeneroEntity{
                Id = Guid.NewGuid(),
                Ativo = true,
                Nome = "Comédia"
            });

            base.OnModelCreating(modelBuilder);
        }

        public void Clear()
        {
            var changedEntries = ChangeTracker.Entries()
                .Where(e => e.State != EntityState.Detached)
                .ToList();

            foreach (var entry in changedEntries)
                Entry(entry.Entity).State = EntityState.Detached;
        }
    }
}
