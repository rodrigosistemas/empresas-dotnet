﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IMDb.Infra.Data.Migrations
{
    public partial class CreateDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "dbo");

            migrationBuilder.CreateTable(
                name: "Ator",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(80)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ator", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Diretor",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(80)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Diretor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Genero",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(80)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genero", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Usuario",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(80)", nullable: false),
                    Email = table.Column<string>(type: "VARCHAR(80)", nullable: false),
                    Senha = table.Column<string>(type: "VARCHAR(32)", maxLength: 32, nullable: false),
                    PerfilAcesso = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Filme",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(type: "VARCHAR(80)", nullable: false),
                    IdGenero = table.Column<Guid>(nullable: false),
                    IdDiretor = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Filme", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Filme_Diretor_IdDiretor",
                        column: x => x.IdDiretor,
                        principalSchema: "dbo",
                        principalTable: "Diretor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Filme_Genero_IdGenero",
                        column: x => x.IdGenero,
                        principalSchema: "dbo",
                        principalTable: "Genero",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Avaliacao",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    IdUsuario = table.Column<Guid>(nullable: false),
                    IdFilme = table.Column<Guid>(nullable: false),
                    Nota = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Avaliacao", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Avaliacao_Filme_IdFilme",
                        column: x => x.IdFilme,
                        principalSchema: "dbo",
                        principalTable: "Filme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Avaliacao_Usuario_IdUsuario",
                        column: x => x.IdUsuario,
                        principalSchema: "dbo",
                        principalTable: "Usuario",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FilmeAtor",
                schema: "dbo",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    IdAtor = table.Column<Guid>(nullable: false),
                    IdFilme = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FilmeAtor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FilmeAtor_Ator_IdAtor",
                        column: x => x.IdAtor,
                        principalSchema: "dbo",
                        principalTable: "Ator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FilmeAtor_Filme_IdFilme",
                        column: x => x.IdFilme,
                        principalSchema: "dbo",
                        principalTable: "Filme",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Ator",
                columns: new[] { "Id", "Ativo", "Nome" },
                values: new object[] { new Guid("8a2aa35a-1e79-4a4b-b5e8-eb5db635efdd"), true, "Rodrigo teste 1" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Diretor",
                columns: new[] { "Id", "Ativo", "Nome" },
                values: new object[] { new Guid("a2617fae-2c16-4cd0-9925-dbe5f2e60914"), true, "Rodrigo teste 1" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Genero",
                columns: new[] { "Id", "Ativo", "Nome" },
                values: new object[] { new Guid("79805b65-7957-4dfd-8aa3-d02134ae371a"), true, "Comédia" });

            migrationBuilder.InsertData(
                schema: "dbo",
                table: "Usuario",
                columns: new[] { "Id", "Ativo", "Email", "Nome", "PerfilAcesso", "Senha" },
                values: new object[] { new Guid("ba820baf-379f-4717-93ce-4b37e8e5ea26"), true, "rodrigo.sistemas.infor@gmail.com", "Rodrigo", 1, "E10ADC3949BA59ABBE56E057F20F883E" });

            migrationBuilder.CreateIndex(
                name: "IX_Avaliacao_IdFilme",
                schema: "dbo",
                table: "Avaliacao",
                column: "IdFilme");

            migrationBuilder.CreateIndex(
                name: "IX_Avaliacao_IdUsuario",
                schema: "dbo",
                table: "Avaliacao",
                column: "IdUsuario");

            migrationBuilder.CreateIndex(
                name: "IX_Filme_IdDiretor",
                schema: "dbo",
                table: "Filme",
                column: "IdDiretor");

            migrationBuilder.CreateIndex(
                name: "IX_Filme_IdGenero",
                schema: "dbo",
                table: "Filme",
                column: "IdGenero");

            migrationBuilder.CreateIndex(
                name: "IX_FilmeAtor_IdAtor",
                schema: "dbo",
                table: "FilmeAtor",
                column: "IdAtor");

            migrationBuilder.CreateIndex(
                name: "IX_FilmeAtor_IdFilme",
                schema: "dbo",
                table: "FilmeAtor",
                column: "IdFilme");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Avaliacao",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "FilmeAtor",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Usuario",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Ator",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Filme",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Diretor",
                schema: "dbo");

            migrationBuilder.DropTable(
                name: "Genero",
                schema: "dbo");
        }
    }
}
