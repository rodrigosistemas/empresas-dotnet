﻿using IMDb.Infra.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace IMDb.Infra.Data.Interface
{
    public interface IContext
    {
        DbSet<UsuarioEntity> Usuario { get; set; }
        DbSet<AtorEntity> Ator { get; set; }
        DbSet<DiretorEntity> Diretor { get; set; }
        DbSet<FilmeEntity> Filme { get; set; }
        DbSet<GeneroEntity> Genero { get; set; }
        DbSet<FilmeAtorEntity> FilmeAtor { get; set; }

        Microsoft.EntityFrameworkCore.Infrastructure.DatabaseFacade Database { get; }
        Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry Entry(object entity);
        Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
        DbContext DbContext { get; }
        void Clear();
    }
}
