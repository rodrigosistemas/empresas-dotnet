﻿using IMDb.Application.Service;
using IMDb.Application.Service.Token;
using IMDb.Domain.Repository;
using IMDb.Domain.Service;
using IMDb.Infra.Data;
using IMDb.Infra.Data.Interface;
using IMDb.Infra.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace IMDb.Infra.CrossCutting
{
    public class BootStrapper
    {
        private static readonly string DATABASE_HOST = Environment.GetEnvironmentVariable("HOST");
        private static readonly string DATABASE_PORT = Environment.GetEnvironmentVariable("PORT");
        private static readonly string DATABASE_USER = Environment.GetEnvironmentVariable("USER");
        private static readonly string DATABASE_PASS = Environment.GetEnvironmentVariable("PASS");
        private static readonly string DATABASE_NAME = Environment.GetEnvironmentVariable("DATABASE_NAME");

        public static void RegisterServices(IServiceCollection services)
        {
            // Service
            services.AddScoped<IUsuarioService, UsuarioService>();
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IFilmeService, FilmeService>();
            services.AddScoped<IAtorService, AtorService>();
            services.AddScoped<IAvaliacaoService, AvaliacaoService>();
            services.AddScoped<IDiretorService, DiretorService>();
            services.AddScoped<IGeneroService, GeneroService>();

            // Repository
            services.AddScoped<IUsuarioRepository, UsuarioRepository>();
            services.AddScoped<IFilmeRepository, FilmeRepository>();
            services.AddScoped<IAtorRepository, AtorRepository>();
            services.AddScoped<IAvaliacaoRepository, AvaliacaoRepository>();
            services.AddScoped<IDiretorRepository, DiretorRepository>();
            services.AddScoped<IGeneroRepository, GeneroRepository>();
            services.AddScoped<IFilmeAtorRepository, FilmeAtorRepository>();

            //connectionString
            var connectionString = $"Server={DATABASE_HOST}{(string.IsNullOrEmpty(DATABASE_PORT) ? "" : "," + DATABASE_PORT)};Database={DATABASE_NAME};User Id={DATABASE_USER};Password={DATABASE_PASS}";

            //Context
            services.AddScoped<IContext, Context>();
            services.AddDbContext<Context>(options => options
                    .UseSqlServer(connectionString, sqlServerOptions => sqlServerOptions.CommandTimeout(30)));

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddHealthChecks()
                    .AddSqlServer(connectionString);
        }
    }
}
