﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Infra.Data.Entities;

namespace IMDb.Infra.CrossCutting
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<AtorEntity, AtorDomain>().ReverseMap();
            CreateMap<AvaliacaoEntity, AvaliacaoDomain>().ReverseMap();
            CreateMap<DiretorEntity, DiretorDomain>().ReverseMap();
            CreateMap<FilmeAtorEntity, FilmeAtorEntity>().ReverseMap();
            CreateMap<FilmeEntity, FilmeDomain>().ReverseMap();
            CreateMap<GeneroEntity, GeneroDomain>().ReverseMap();
            CreateMap<UsuarioEntity, UsuarioDomain>().ReverseMap();
        }
    }
}
