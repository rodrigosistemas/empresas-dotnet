﻿using AutoMapper;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using IMDb.Presentation.UI.REST.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST
{
    [Route("api/[controller]")]
    [ApiController]
    public class GeneroController : ControllerBase
    {
        private readonly IGeneroService _service;
        protected readonly IMapper _mapper;

        public GeneroController(IGeneroService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(GeneroVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<GeneroVM> Get(Guid id)
        {
            var genero = _service.GetById(id);

            if (genero == null)
                return NotFound();

            return _mapper.Map<GeneroVM>(genero);
        }

        [HttpPost("list")]
        [Authorize]
        [ProducesResponseType(typeof(ListVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult List([FromBody] GeneroSeletor seletor)
        {
            if (seletor == null)
               throw new Exception("Filtro inválido");

            return Ok(new ListVM(
                       data: _mapper.Map<IEnumerable<GeneroVM>>(_service.GetList(seletor)),
                       count: _service.Count(seletor)));
        }
    }
}
