﻿using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class FilmeDetalheVM : BaseVM
    {
        public string Nome { get; set; }
        public GeneroVM Genero { get; set; }
        public DiretorVM Diretor { get; set; }
        public IEnumerable<AtorVM> Atores { get; set; }
        public decimal Avaliacao { get; set; }
    }
}
