﻿using System;

namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class BaseVM
    {
        public Guid Id { get; set; }
    }
}
