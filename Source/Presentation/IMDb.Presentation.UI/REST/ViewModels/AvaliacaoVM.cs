﻿using System;

namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class AvaliacaoVM : BaseVM
    {
        public Guid IdUsuario { get; set; }
        public Guid IdFilme { get; set; }
        public int Nota { get; set; }
    }
}
