﻿using IMDb.Domain.Enums;

namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class UsuarioVM : BaseVM
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public PerfilAcesso PerfilAcesso { get; set; }
    }
}
