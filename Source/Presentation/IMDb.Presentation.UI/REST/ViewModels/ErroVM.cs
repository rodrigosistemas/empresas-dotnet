﻿namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class ErroVM
    {
        public ErroVM(){ }

        public ErroVM(string chave, string erro)
        {
            Chave = chave;
            Erro = erro;
        }

        public string Chave { get; set; }
        public string Erro { get; set; }
    }
}
