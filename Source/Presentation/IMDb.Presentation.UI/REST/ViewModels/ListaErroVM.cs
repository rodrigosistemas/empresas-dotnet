﻿using prmToolkit.NotificationPattern;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class ListaErroVM
    {
        public ListaErroVM() { }
        public ListaErroVM(IReadOnlyCollection<Notification> notifications) {
            Erros = notifications.Select(x => new ErroVM(x.Property, x.Message)); 
        }

        public IEnumerable<ErroVM> Erros { get; set; }
    }
}
