﻿using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class ListVM
    {
        public ListVM(IEnumerable<object> data, int count)
        {
            Data = data;
            Count = count;
        }

        public IEnumerable<object> Data { get; set; }
        public int Count { get; set; }
    }
}
