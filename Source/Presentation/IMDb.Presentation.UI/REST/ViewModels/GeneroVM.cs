﻿namespace IMDb.Presentation.UI.REST.ViewModels
{
    public class GeneroVM : BaseVM
    {
        public string Nome { get; set; }
    }
}
