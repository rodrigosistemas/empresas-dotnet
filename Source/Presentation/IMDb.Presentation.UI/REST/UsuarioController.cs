﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Constante;
using IMDb.Domain.Extensions;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using IMDb.Presentation.UI.REST.Payloads;
using IMDb.Presentation.UI.REST.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioService _service;
        protected readonly IMapper _mapper;

        public UsuarioController(IUsuarioService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(UsuarioVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<UsuarioVM> Get(Guid id)
        {
            var user = _service.GetById(id);

            if (user == null)
                return NotFound();

            return _mapper.Map<UsuarioVM>(user);
        }

        [HttpPost("list")]
        [Authorize]
        [ProducesResponseType(typeof(ListVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult List([FromBody] UsuarioSeletor seletor)
        {
            if (seletor == null)
               throw new Exception("Filtro inválido");

            return Ok(new ListVM(
                       data: _mapper.Map<IEnumerable<UsuarioVM>>(_service.GetList(seletor)),
                       count: _service.Count(seletor)));
        }

        [HttpPost("list-perfil-usuario")]
        [Authorize(Roles = UsuarioConstante.Administrador)]
        [ProducesResponseType(typeof(ListVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult ListPerfilUsuario([FromBody] UsuarioComunSeletor seletor)
        {
            if (seletor == null)
                throw new Exception("Filtro inválido");

            return Ok(new ListVM(
                       data: _mapper.Map<IEnumerable<UsuarioVM>>(_service.GetListPerfilUsuario(seletor)),
                       count: _service.CountPerfilUsuario(seletor)));
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(typeof(UsuarioVM), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ListaErroVM), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Insert([FromBody] Usuario user)
        {
            if (user == null)
               return BadRequest(new ListaErroVM() { Erros = new List<ErroVM> { new ErroVM("Usuário", "Usuário inválido") } });

           var domain = new UsuarioDomain(user.Nome, user.Email, user.Senha.ToMD5(), user.PerfilAcesso);
           if(domain.IsInvalid())
               return BadRequest(new ListaErroVM(domain.Notifications));

            domain = _service.Insert(domain);
            return CreatedAtAction(nameof(Get),new { id = domain.Id }, _mapper.Map<UsuarioVM>(domain));
        }

        [HttpDelete]
        [Authorize]
        public ActionResult Delete(Guid id)
        {
           _service.Delete(id);
           return Ok();
        }

        [HttpPost("Login")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(typeof(ListaErroVM),StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(ListaErroVM),404)]
        public ActionResult Login([FromBody] Login user, [FromServices] ITokenService _tokenService)
        {
            if (user == null)
                return BadRequest(new ListaErroVM() { Erros = new List<ErroVM> { new ErroVM("Usuário","Usuário ou senha inválidos") } });

            var userDomain = new UsuarioDomain(user.Email, user.Senha.ToMD5());

            if (userDomain.IsInvalid())
                return BadRequest(new ListaErroVM(userDomain.Notifications));

            var userLogado = _service.Login(userDomain);

            if(userLogado == null)
                return NotFound(new ListaErroVM() { Erros = new List<ErroVM> { new ErroVM("Usuário", "Usuário ou senha inválidos") } });
            
            return Ok(new { token = _tokenService.GenerateToken(userLogado) });
        }
    }
}
