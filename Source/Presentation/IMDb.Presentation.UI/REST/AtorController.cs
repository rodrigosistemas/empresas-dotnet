﻿using AutoMapper;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using IMDb.Presentation.UI.REST.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST
{
    [Route("api/[controller]")]
    [ApiController]
    public class AtorController : ControllerBase
    {
        private readonly IAtorService _service;
        protected readonly IMapper _mapper;

        public AtorController(IAtorService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(AtorVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<AtorVM> Get(Guid id)
        {
            var ator = _service.GetById(id);

            if (ator == null)
                return NotFound();

            return _mapper.Map<AtorVM>(ator);
        }

        [HttpPost("list")]
        [Authorize]
        [ProducesResponseType(typeof(ListVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult List([FromBody] AtorSeletor seletor)
        {
            if (seletor == null)
               throw new Exception("Filtro inválido");

            return Ok(new ListVM(
                       data: _mapper.Map<IEnumerable<AtorVM>>(_service.GetList(seletor)),
                       count: _service.Count(seletor)));
        }
    }
}
