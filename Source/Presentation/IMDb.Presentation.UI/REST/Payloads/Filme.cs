﻿using System;
using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST.Payloads
{
    public class Filme
    {
        public string Nome { get; set; }
        public Guid IdGenero { get; set; }
        public Guid IdDiretor { get; set; }
        public IEnumerable<Ator> Atores { get; set; }
    }
}
