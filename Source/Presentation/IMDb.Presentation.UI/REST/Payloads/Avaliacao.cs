﻿using System;

namespace IMDb.Presentation.UI.REST.Payloads
{
    public class Avaliacao
    {
        public Guid IdUsuario { get; set; }
        public Guid IdFilme { get; set; }
        public int Nota { get; set; }
    }
}
