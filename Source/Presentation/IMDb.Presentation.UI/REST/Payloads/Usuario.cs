﻿using IMDb.Domain.Enums;

namespace IMDb.Presentation.UI.REST.Payloads
{
    public class Usuario
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public PerfilAcesso PerfilAcesso { get; set; }
    }
}
