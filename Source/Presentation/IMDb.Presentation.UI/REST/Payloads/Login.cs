﻿namespace IMDb.Presentation.UI.REST.Payloads
{
    public class Login
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
