﻿using AutoMapper;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using IMDb.Presentation.UI.REST.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace IMDb.Presentation.UI.REST
{
    [Route("api/[controller]")]
    [ApiController]
    public class DiretorController : ControllerBase
    {
        private readonly IDiretorService _service;
        protected readonly IMapper _mapper;

        public DiretorController(IDiretorService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(DiretorVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<DiretorVM> Get(Guid id)
        {
            var diretor = _service.GetById(id);

            if (diretor == null)
                return NotFound();

            return _mapper.Map<DiretorVM>(diretor);
        }

        [HttpPost("list")]
        [Authorize]
        [ProducesResponseType(typeof(ListVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult List([FromBody] DiretorSeletor seletor)
        {
            if (seletor == null)
               throw new Exception("Filtro inválido");

            return Ok(new ListVM(
                       data: _mapper.Map<IEnumerable<DiretorVM>>(_service.GetList(seletor)),
                       count: _service.Count(seletor)));
        }
    }
}
