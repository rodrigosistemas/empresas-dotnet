﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Constante;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using IMDb.Presentation.UI.REST.Payloads;
using IMDb.Presentation.UI.REST.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Presentation.UI.REST
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilmeController : ControllerBase
    {
        private readonly IFilmeService _service;
        protected readonly IMapper _mapper;

        public FilmeController(IFilmeService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(FilmeVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<FilmeVM> Get(Guid id)
        {
            var filme = _service.GetById(id);

            if (filme == null)
                return null;

            return _mapper.Map<FilmeVM>(filme);
        }

        [HttpPost("list")]
        [ProducesResponseType(typeof(BaseVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult List([FromBody] FilmeSeletor seletor)
        {
            if (seletor == null)
               throw new Exception("Filtro inválido");

            return Ok(new ListVM(
                       data: _mapper.Map<IEnumerable<FilmeVM>>(_service.GetList(seletor)),
                       count: _service.Count(seletor)));
        }
        
        [HttpPost]
        [Authorize(Roles = UsuarioConstante.Administrador)]
        [ProducesResponseType(typeof(FilmeVM), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ListaErroVM), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Insert([FromBody] Filme filme)
        {
            if (filme == null)
               return BadRequest(new ListaErroVM() { Erros = new List<ErroVM> { new ErroVM("Usuário", "Usuário inválido") } });

           var atores = filme.Atores != null && filme.Atores.Any() ? filme.Atores.Select(x => new AtorDomain(x.Id)) : null;
           var domain = new FilmeDomain(filme.Nome, filme.IdGenero, filme.IdDiretor, atores);
           
           if(domain.IsInvalid())
               return BadRequest(new ListaErroVM(domain.Notifications));
            
            domain = _service.Insert(domain);
            return CreatedAtAction(nameof(Get), new { id = domain.Id }, _mapper.Map<FilmeVM>(domain));
        }

        [HttpGet("Detalhe/{id}")]
        [Authorize]
        [ProducesResponseType(typeof(FilmeVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<FilmeDetalheVM> GetDetalhe(Guid id)
        {
            var filme = _service.GetFilmeDetalheById(id);

            if (filme == null)
                return null;

            return _mapper.Map<FilmeDetalheVM>(filme);
        }
    }
}
