﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Domain.Service;
using IMDb.Presentation.UI.REST.Payloads;
using IMDb.Presentation.UI.REST.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace IMDb.Presentation.UI.REST
{
    [Route("api/[controller]")]
    [ApiController]
    public class AvaliacaoController : ControllerBase
    {
        private readonly IAvaliacaoService _service;
        protected readonly IMapper _mapper;

        public AvaliacaoController(IAvaliacaoService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        [HttpGet("{id}")]
        [Authorize]
        [ProducesResponseType(typeof(AvaliacaoVM), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult<AvaliacaoVM> Get(Guid id)
        {
            var avaliacao = _service.GetById(id);

            if (avaliacao == null)
                return null;

            return _mapper.Map<AvaliacaoVM>(avaliacao);
        }

        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(ListaErroVM), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public ActionResult Post(Avaliacao Avaliacao)
        {
            if (Avaliacao == null)
                return BadRequest();

            var domain = new AvaliacaoDomain(Avaliacao.IdUsuario, Avaliacao.IdFilme, Avaliacao.Nota);

            if(domain.IsInvalid())
                return BadRequest(new ListaErroVM(domain.Notifications));

            var avaliacaoVM = _mapper.Map<AvaliacaoVM>(_service.Insert(domain));
            
            return CreatedAtAction(nameof(Get), new { avaliacaoVM.Id }, avaliacaoVM);
        }
    }
}
