﻿using AutoMapper;
using IMDb.Domain;
using IMDb.Presentation.UI.REST.ViewModels;

namespace IMDb.Presentation.UI.Config
{
    public class AutoMapperProfileVM : Profile
    {
        public AutoMapperProfileVM()
        {
            CreateMap<UsuarioDomain, UsuarioVM>().ReverseMap();
            CreateMap<FilmeDomain, FilmeVM>().ReverseMap();
            CreateMap<AvaliacaoDomain, AvaliacaoVM>().ReverseMap();
            CreateMap<DiretorDomain, DiretorVM>().ReverseMap();
            CreateMap<AtorDomain, AtorVM>().ReverseMap();
            CreateMap<GeneroDomain, GeneroVM>().ReverseMap();
            CreateMap<FilmeDomain, FilmeDetalheVM>().ReverseMap();
        }
    }
}
