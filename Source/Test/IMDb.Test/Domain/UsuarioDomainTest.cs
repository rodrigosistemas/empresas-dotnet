﻿using IMDb.Domain;
using Xunit;

namespace IMDb.Test.Domain
{
    public class UsuarioDomainTest
    {
        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void ValidarNomeObrigatorio(string nome)
        {
            var usuario = new UsuarioDomain();
            usuario.ValidarNome(nome);

            Assert.True(usuario.IsInvalid());
        }

        [Theory]
        [InlineData("rWzR9oDbZAG3wO6UWTF3XJLyoPdmFjAx60F5eJW4Jdx5rtQDDW7agIOKYsxeRyRNnXJIggDq90mqk3uoO")]
        [InlineData("r7YYlb4QAWNSJd2CQLzIB69FhLWpbMpJKEhwfbCfyQz7uqAB2zK4wJ9TWW9yjp7ttffP7iww0kskQtmJi")]
        public void ValidaNomeMaiorQue80Caracteres(string nome)
        {
            var usuario = new UsuarioDomain();
            usuario.ValidarNome(nome);

            Assert.True(usuario.IsInvalid());
        }

        [Theory]
        [InlineData("Teste")]
        [InlineData("Teste 1")]
        public void ValidaNome(string nome)
        {
            var usuario = new UsuarioDomain();
            usuario.ValidarNome(nome);

            Assert.True(usuario.IsValid());
        }

        //Validar todos os atributos...
    }
}
