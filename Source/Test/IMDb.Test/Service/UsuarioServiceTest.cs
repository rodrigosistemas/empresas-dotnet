﻿using IMDb.Application.Service;
using IMDb.Domain;
using IMDb.Domain.Extensions;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using NSubstitute;
using System;
using System.Collections.Generic;
using Xunit;

namespace IMDb.Test.Service
{
    public class UsuarioServiceTest
    {
        private IUsuarioRepository _mockRepository;
        private IUsuarioService _service;

        private const string EmailMock = "teste@gmail.com";
        private const string SenhaMock = "123456";

        public UsuarioServiceTest()
        {
            _mockRepository = Substitute.For<IUsuarioRepository>();
            _mockRepository.GetList(new UsuarioSeletor { }).ReturnsForAnyArgs(new List<UsuarioDomain>
            {
              new UsuarioDomain
              {
                  Id = Guid.NewGuid(),
                  Nome = "Teste Rodrigo",
                  Senha = "E10ADC3949BA59ABBE56E057F20F883E",
                  Ativo = true,
                  Email = "teste@gmail.com"
              }
            }) ;
            _service = new UsuarioService(_mockRepository);
        }

        [Theory]
        [InlineData("teste@gmail.com","123456")]
        public void ValidaLogin(string email, string senha)
        {
            var domain = new UsuarioDomain(email, senha.ToMD5());
            var result = _service.Login(domain);

            Assert.True(domain.IsValid());
            Assert.NotNull(result);
            Assert.Equal(email, result.Email);
        }

        //Validar todos o metodos...
    }
}
