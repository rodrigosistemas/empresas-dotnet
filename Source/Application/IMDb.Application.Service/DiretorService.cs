﻿using IMDb.Application.Service.Abstract;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using System;

namespace IMDb.Application.Service
{
    public class DiretorService : ServiceBase<IDiretorRepository, DiretorDomain, DiretorSeletor>, IDiretorService
    {
        public DiretorService(IDiretorRepository repository) : base(repository) { }

        public void Delete(Guid id)
        {
            throw new System.NotImplementedException();
        }
        public override DiretorDomain Update(DiretorDomain domain)
        {
            throw new System.NotImplementedException();
        }
    }
}
