﻿using IMDb.Application.Service.Abstract;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using System;

namespace IMDb.Application.Service
{
    public class GeneroService : ServiceBase<IGeneroRepository, GeneroDomain, GeneroSeletor>, IGeneroService
    {
        public GeneroService(IGeneroRepository repository) : base(repository) { }

        public void Delete(Guid id)
        {
            throw new System.NotImplementedException();
        }
        public override GeneroDomain Update(GeneroDomain domain)
        {
            throw new System.NotImplementedException();
        }
        public override GeneroDomain Insert(GeneroDomain domain)
        {
            return base.Insert(domain);
        }
    }
}
