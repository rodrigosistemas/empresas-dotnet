﻿using IMDb.Application.Service.Abstract;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Application.Service
{
    public class FilmeService : ServiceBase<IFilmeRepository, FilmeDomain, FilmeSeletor>, IFilmeService
    {
        private readonly IFilmeAtorRepository _filmeAtorRepository;

        public FilmeService(IFilmeRepository repository, IFilmeAtorRepository filmeAtorRepository) 
        : base(repository)
        {
            _filmeAtorRepository = filmeAtorRepository;
        }

        public void Delete(Guid id)
        {
            throw new System.NotImplementedException();
        }
        public override FilmeDomain Update(FilmeDomain domain)
        {
            throw new System.NotImplementedException();
        }
        public override FilmeDomain Insert(FilmeDomain domain)
        {
            FilmeDomain domainInsert = base.Insert(domain);

            if (domain.Atores != null && domain.Atores.Any())
            {
                IEnumerable<FilmeAtorDomain> filmeAtores = domain.Atores.Select(ator => new FilmeAtorDomain(ator.Id, domainInsert.Id));
                _filmeAtorRepository.InsertManyReturningObject(filmeAtores);
            }

            return domainInsert;
        }

        public override IEnumerable<FilmeDomain> GetList(FilmeSeletor seletor)
            => _repository.GetListMedia(seletor);

        public FilmeDomain GetFilmeDetalheById(Guid id)
            => _repository.GetFilmeDetalheById(id);
    }
}
