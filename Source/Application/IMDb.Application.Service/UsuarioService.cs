﻿using IMDb.Application.Service.Abstract;
using IMDb.Domain;
using IMDb.Domain.Enums;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IMDb.Application.Service
{
    public class UsuarioService : ServiceBase<IUsuarioRepository, UsuarioDomain, UsuarioSeletor>, IUsuarioService
    {
        public UsuarioService(IUsuarioRepository repository) : base(repository) { }

        public void Delete(Guid id)
            => _repository.Desativar(id);
        
        public override UsuarioDomain Update(UsuarioDomain domain)
        {
            throw new System.NotImplementedException();
        }
        public override UsuarioDomain Insert(UsuarioDomain domain)
        {
            return base.Insert(domain);
        }
        public UsuarioDomain Login(UsuarioDomain domain)
        {
            if (domain.IsInvalid())
                return null;

            return _repository.GetList(new UsuarioSeletor { Email = domain.Email, Senha = domain.Senha }).FirstOrDefault();
        }

        public IEnumerable<UsuarioDomain> GetListPerfilUsuario(UsuarioComunSeletor seletor)
        {
            var seletorUsuario = new UsuarioSeletor
            {
                RegistroPorPagina = seletor.RegistroPorPagina,
                Pagina = seletor.Pagina,
                Ativo = true,
                PerfilAcesso = PerfilAcesso.Usuario,
                OrderBy = nameof(UsuarioDomain.Nome)
            };

           return _repository.GetList(seletorUsuario);
        }

        public int CountPerfilUsuario(SeletorBase seletor)
        {
            var seletorUsuario = new UsuarioSeletor
            {
                Ativo = true,
                PerfilAcesso = PerfilAcesso.Usuario,
            };

            return _repository.Count(seletorUsuario);
        }
    }
}
