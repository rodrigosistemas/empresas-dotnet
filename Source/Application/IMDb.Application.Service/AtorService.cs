﻿using IMDb.Application.Service.Abstract;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using System;

namespace IMDb.Application.Service
{
    public class AtorService : ServiceBase<IAtorRepository, AtorDomain, AtorSeletor>, IAtorService
    {
        public AtorService(IAtorRepository repository) : base(repository) { }

        public void Delete(Guid id)
        {
            throw new System.NotImplementedException();
        }
        public override AtorDomain Update(AtorDomain domain)
        {
            throw new System.NotImplementedException();
        }
    }
}
