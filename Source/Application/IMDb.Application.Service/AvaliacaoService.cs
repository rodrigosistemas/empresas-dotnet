﻿using IMDb.Application.Service.Abstract;
using IMDb.Domain;
using IMDb.Domain.Repository;
using IMDb.Domain.Seletores;
using IMDb.Domain.Service;
using System;

namespace IMDb.Application.Service
{
    public class AvaliacaoService : ServiceBase<IAvaliacaoRepository, AvaliacaoDomain, AvaliacaoSeletor>, IAvaliacaoService
    {
        public AvaliacaoService(IAvaliacaoRepository repository) : base(repository) { }

        public void Delete(Guid id)
        {
            throw new System.NotImplementedException();
        }
        public override AvaliacaoDomain Update(AvaliacaoDomain domain)
        {
            throw new System.NotImplementedException();
        }
    }
}
